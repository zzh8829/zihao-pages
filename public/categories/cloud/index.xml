<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Cloud on Zihao Zhang</title>
    <link>https://zihao.me/categories/cloud/</link>
    <description>Recent content in Cloud on Zihao Zhang</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <copyright>Copyright © 2017 Zihao Zhang &lt;a rel=&#34;license&#34; href=&#34;https://creativecommons.org/licenses/by-nc-sa/4.0//&#34;&gt;CC BY-NC-SA 4.0&lt;/a&gt;</copyright>
    <lastBuildDate>Sun, 23 Apr 2017 23:26:07 -0700</lastBuildDate>
    <atom:link href="https://zihao.me/categories/cloud/" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Custom Nginx Ingress Controller on Google Container Engine</title>
      <link>https://zihao.me/post/cheap-out-google-container-engine-load-balancer/</link>
      <pubDate>Sun, 23 Apr 2017 23:26:07 -0700</pubDate>
      
      <guid>https://zihao.me/post/cheap-out-google-container-engine-load-balancer/</guid>
      <description>&lt;p&gt;Google Container Engine offers a great managed kubernetes cluster. But it comes with one catch, Load Balancing and Ingress Controller are rather expensive. Here is how you can use nginx as an alternative to google&amp;rsquo;s load balancer.
&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;export CLUSTER_NAME=zihao

# create load balancer&#39;s static ip
gcloud compute addresses create $CLUSTER_NAME-ip --region us-west1
export LB_ADDRESS_IP=$(gcloud compute addresses list | grep $CLUSTER_NAME-ip | awk &#39;{print $3}&#39;)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;We want our load balancer node to have a static IP address so we don&amp;rsquo;t need to change DNS.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;# create cluster with one instance
gcloud container clusters create $CLUSTER_NAME --disable-addons HttpLoadBalancing --disk-size=30 --machine-type=g1-small --num-nodes=1
gcloud container clusters get-credentials $CLUSTER_NAME

# re-assign static ip to instance
export LB_INSTANCE_NAME=$(kubectl describe nodes | head -n1 | awk &#39;{print $2}&#39;)
export LB_INSTANCE_NAT=$(gcloud compute instances describe $LB_INSTANCE_NAME | grep -A3 networkInterfaces: | tail -n1 | awk -F&#39;: &#39; &#39;{print $2}&#39;)
gcloud compute instances delete-access-config $LB_INSTANCE_NAME \
    --access-config-name &amp;quot;$LB_INSTANCE_NAT&amp;quot;
gcloud compute instances add-access-config $LB_INSTANCE_NAME \
    --access-config-name &amp;quot;$LB_INSTANCE_NAT&amp;quot; --address $LB_ADDRESS_IP
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;We want to label our load balancing node so kubernetes only assigns nginx ingress controller to this node&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;# label our load balancer node
kubectl label nodes $LB_INSTANCE_NAME role=load-balancer

# enable http ports for load balancer
gcloud compute instances add-tags $LB_INSTANCE_NAME --tags http-server,https-server
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now we have everything set up on google cloud, here is the deployment file for nginx ingress controller.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-YAML&#34;&gt;apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: default-http-backend
  labels:
    k8s-app: default-http-backend
  namespace: kube-system
spec:
  replicas: 1
  template:
    metadata:
      labels:
        k8s-app: default-http-backend
    spec:
      terminationGracePeriodSeconds: 60
      containers:
      - name: default-http-backend
        # Any image is permissable as long as:
        # 1. It serves a 404 page at /
        # 2. It serves 200 on a /healthz endpoint
        image: gcr.io/google_containers/defaultbackend:1.0
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8080
            scheme: HTTP
          initialDelaySeconds: 30
          timeoutSeconds: 5
        ports:
        - containerPort: 8080
        resources:
          limits:
            cpu: 10m
            memory: 20Mi
          requests:
            cpu: 10m
            memory: 20Mi
---
apiVersion: v1
kind: Service
metadata:
  name: default-http-backend
  namespace: kube-system
  labels:
    k8s-app: default-http-backend
spec:
  ports:
  - port: 80
    targetPort: 8080
  selector:
    k8s-app: default-http-backend
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-ingress-controller
  namespace: kube-system
data:
  hsts-include-subdomains: &amp;quot;false&amp;quot;
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nginx-ingress-controller
  labels:
    k8s-app: nginx-ingress-controller
  namespace: kube-system
spec:
  replicas: 1
  template:
    metadata:
      labels:
        k8s-app: nginx-ingress-controller
    spec:
      # hostNetwork makes it possible to use ipv6 and to preserve the source IP correctly regardless of docker configuration
      # however, it is not a hard dependency of the nginx-ingress-controller itself and it may cause issues if port 10254 already is taken on the host
      # that said, since hostPort is broken on CNI (https://github.com/kubernetes/kubernetes/issues/31307) we have to use hostNetwork where CNI is used
      # like with kubeadm
      hostNetwork: true
      terminationGracePeriodSeconds: 60
      nodeSelector:
        role: load-balancer
      containers:
      - image: gcr.io/google_containers/nginx-ingress-controller:0.9.0-beta.3
        name: nginx-ingress-controller
        readinessProbe:
          httpGet:
            path: /healthz
            port: 10254
            scheme: HTTP
        livenessProbe:
          httpGet:
            path: /healthz
            port: 10254
            scheme: HTTP
          initialDelaySeconds: 10
          timeoutSeconds: 1
        ports:
        - containerPort: 80
          hostPort: 80
        - containerPort: 443
          hostPort: 443
        env:
          - name: POD_NAME
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: POD_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
        args:
        - /nginx-ingress-controller
        - --default-backend-service=$(POD_NAMESPACE)/default-http-backend
        - --configmap=$(POD_NAMESPACE)/nginx-ingress-controller
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Here we have `&lt;code&gt;nodeSelector: role: load-balancer&lt;/code&gt; so this pod only lands on load balancer node in the cluster. We use &lt;code&gt;hostPort&lt;/code&gt; here to connect internet traffic with nginx container.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;# install nginx ingres controller with hostPort
kubectl apply -f nginx-ingress-controller.yaml
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;You can test the default backend with &lt;code&gt;curl $LB_ADDRESS_IP&lt;/code&gt; and should see output like &lt;code&gt;default backend - 404&lt;/code&gt;. If everything goes as expected, our cheap ingress controller is ready to use for Ingress resource with &lt;code&gt;kubernetes.io/ingress.class: &amp;quot;nginx&amp;quot;&lt;/code&gt; annotation.&lt;/p&gt;</description>
    </item>
    
    <item>
      <title>Creating a Kubernetes Cluster from Scratch with Kubeadm</title>
      <link>https://zihao.me/post/creating-a-kubernetes-cluster-from-scratch-with-kubeadm/</link>
      <pubDate>Sun, 19 Mar 2017 10:39:54 -0700</pubDate>
      
      <guid>https://zihao.me/post/creating-a-kubernetes-cluster-from-scratch-with-kubeadm/</guid>
      <description>&lt;p&gt;Containerization and Kubernetes are the hottest cloud technologies right now. Here is how I configured a mini Kubernetes cluster for my side projects.
&lt;/p&gt;

&lt;h2 id=&#34;an-introduction-to-kubernetes&#34;&gt;An Introduction to Kubernetes&lt;/h2&gt;

&lt;p&gt;You are probably already familiar with docker containers, simply running &lt;code&gt;docker build&lt;/code&gt; will get you a consistent and reusable deployment unit. Everything sounds great, but manually deploying, restarting on crash, deciding which container goes to which server and managing their IPs and ports are inconvenient and sometimes painful. Kubernetes is an open source container orchestration solution aiming to solve all of those and more.&lt;/p&gt;

&lt;p&gt;Here are some important Kubernetes term you should know about&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;pod - group of one or more containers running on a &lt;em&gt;node&lt;/em&gt;&lt;/li&gt;
&lt;li&gt;node - a server inside of a Kubernetes &lt;em&gt;cluster&lt;/em&gt;&lt;/li&gt;
&lt;li&gt;cluster - a group of servers managed by Kubernetes&lt;/li&gt;
&lt;li&gt;master - the server that runs Kubernetes and manages other servers&lt;/li&gt;
&lt;li&gt;worker - servers managed by the master server&lt;/li&gt;
&lt;li&gt;deployment - settings for deploying &lt;em&gt;pods&lt;/em&gt; to &lt;em&gt;cluster&lt;/em&gt;&lt;/li&gt;
&lt;li&gt;service - settings for accessing &lt;em&gt;pods&lt;/em&gt; within &lt;em&gt;cluster&lt;/em&gt;&lt;/li&gt;
&lt;li&gt;ingress - settings for accessing &lt;em&gt;pods&lt;/em&gt; from the Internet&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;br/&gt;
A standard Kubernetes cluster has following &lt;a href=&#34;https://kubernetes.io/docs/concepts/overview/components/&#34;&gt;components&lt;/a&gt;&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;etcd - Distributed key-value store for configuration and service discovery.&lt;/li&gt;
&lt;li&gt;weave/flannel - Container Network Interface for connecting services&lt;/li&gt;
&lt;li&gt;kube-apiserver - API server for management and orchestration&lt;/li&gt;
&lt;li&gt;kube-controller-manager - Controls Kubernetes services&lt;/li&gt;
&lt;li&gt;kube-discovery - Service discovery&lt;/li&gt;
&lt;li&gt;kube-dns - DNS server for internal hostnames&lt;/li&gt;
&lt;li&gt;kube-proxy - Routes traffic through proxy&lt;/li&gt;
&lt;li&gt;kube-schedular - Schedules containers on the cluster.&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;br/&gt;
Note that these are my own explanation of what they do, if you want the legitimate and precise definition read the &lt;a href=&#34;https://kubernetes.io/docs&#34;&gt;official docs&lt;/a&gt;. Thanks to Google and the Kubernetes community, setting all of them up takes less than 5 minutes.&lt;/p&gt;

&lt;h2 id=&#34;setting-up-kubernetes-cluster&#34;&gt;Setting up Kubernetes Cluster&lt;/h2&gt;

&lt;p&gt;In this article we are building a minimalistic single node cluster with optional workers. The default CPU/memory configuration for master servers are quite high because they were prepared for larger cluster. I recommend 2 CPUs and 1 GB memory for the least resistant process. If you are cheap like me, you can hack through with some extra tweaks on a low-end server. Any kind of server is fine for workers, as long as they can connect to the master.&lt;/p&gt;

&lt;p&gt;We will start with deploying the Kubernetes master on a fresh ubuntu server. Having some swap space is great, especially if you are low on memory.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;sudo fallocate -l 1G /swapfile
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo echo &amp;quot;/swapfile   none    swap    sw    0   0&amp;quot; &amp;gt;&amp;gt; /etc/fstab
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Install packages required for Kubernetes&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;# Prepare for new repos
sudo apt-get -y install apt-transport-https ca-certificates software-properties-common curl
# Add docker repo
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
       &amp;quot;deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable&amp;quot;
# Add kubernetes repo
sudo curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
sudo add-apt-repository \
       &amp;quot;deb https://apt.kubernetes.io/ \
       kubernetes-$(lsb_release -cs) \
       main&amp;quot;
sudo apt-get update &amp;amp;&amp;amp; apt-get install -y docker-ce kubelet kubeadm kubernetes-cni
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now we can start building the master with kubeadm&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;kubeadm init
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This will print the secret for authenticating worker nodes, don&amp;rsquo;t forget to save them.
If nothing went wrong, the master server will be running! At this point we don&amp;rsquo;t ever need to login to the master server again, the next step will be on your own machine. You need &lt;code&gt;kubectl&lt;/code&gt; to communicate with the master server. On mac with homebrew, this is very simple just &lt;code&gt;brew install kubectl&lt;/code&gt;, for other OS follow the &lt;a href=&#34;https://kubernetes.io/docs/tasks/kubectl/install/&#34;&gt;instructions here&lt;/a&gt;&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;# Download kubectl config from the master server
scp root@&amp;quot;master ip&amp;quot;:/etc/kubernetes/admin.conf ~/.kube/config
# We need this to run pods on master node cuz servers are expensive
kubectl taint nodes --all dedicated-
# Install weave CNI, there are other choices, but weave is probably the easist
kubectl apply -f https://git.io/weave-kube-1.6
# Install Dashboard for nice graphical web interface.
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/kubernetes-dashboard.yaml
# Proxy dashboard so we can view it locally
kubectl proxy
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Congratulations, now we have a complete working single node Kubernetes &lt;em&gt;cluster&lt;/em&gt;. You can check the status and explore a bit with dashboard at &lt;code&gt;localhost:8001/ui&lt;/code&gt;
&lt;img src=&#34;https://zihao.me/images/kube-dash.png&#34; alt=&#34;Kubernetes Dashboard&#34; /&gt;&lt;/p&gt;

&lt;p&gt;To join more nodes into our cluster, repeat the same commands as the master node above, except the &lt;code&gt;kubeadm init&lt;/code&gt; is replaced with&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;kubeadm join --token=&amp;quot;master token&amp;quot; &amp;quot;master ip&amp;quot;
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;You shouldn&amp;rsquo;t need to manually type this, the master server should print this with pre-filled token and IP at the end of initialization.&lt;/p&gt;

&lt;h2 id=&#34;ingress-let-s-encrypt-and-hello-world-deployment&#34;&gt;Ingress, Let&amp;rsquo;s Encrypt and Hello World Deployment&lt;/h2&gt;

&lt;p&gt;Now we have our server cluster running, let&amp;rsquo;s put some real work to it. One thing we haven&amp;rsquo;t talked about is how does our app inside the cluster talks to the Internet? If you are running on AWS or GCE, you can use their built-in load balancer for Kubernetes, but since we are building from scratch, we need a thing called Ingress Controller.&lt;/p&gt;

&lt;p&gt;Ingress Controller controls load balancing, routing and public HTTPS encryption. Each app needs to define its own Ingress resource which I will cover later. To set up an Ingress Controller you can use my configurations here or read up on fancier versions on official docs&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;kubectl apply -f https://gist.githubusercontent.com/zzh8829/fe2e8388a22ec6f2244ccb835b62e07c/raw/4284e123937f5d09683d9a7494d40335e819ccea/nginx-ingress.yaml
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This will create a nginx powered ingress that direct all traffic a default back-end that returns 404 for everything. In order to support encrypted HTTPS traffic, we can use &lt;a href=&#34;https://github.com/jetstack/kube-lego&#34;&gt;kube-lego&lt;/a&gt; to automatically enable HTTPS through Let&amp;rsquo;s Encrypt. Simply download configurations from their &lt;a href=&#34;https://github.com/jetstack/kube-lego/tree/master/examples/nginx/lego&#34;&gt;examples&lt;/a&gt;, modify them and deploy with &lt;code&gt;kubectl apply -f yourconfigname.yaml&lt;/code&gt;&lt;/p&gt;

&lt;p&gt;Now we will create and deploy a basic node.js hello world application. If you don&amp;rsquo;t understand what this code does, just close this page and go back to Reddit or something.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-javascript&#34;&gt;// index.js
var http = require(&#39;http&#39;);
var server = http.createServer(function (request, response) {
  response.writeHead(200, {&amp;quot;Content-Type&amp;quot;: &amp;quot;text/plain&amp;quot;});
  response.end(&amp;quot;Hello World!\n&amp;quot;);
});
server.listen(8000);
console.log(&amp;quot;Server running at http://127.0.0.1:8000/&amp;quot;);
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Create a simple Dockerfile&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;FROM node:boron
RUN mkdir -p /app
WORKDIR /app
COPY . .
EXPOSE 8000
CMD node index.js
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Build and verify and upload to Docker hub&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;docker built -t zihao/hello .
docker run -d -p 8000:8000 zihao/hello
curl localhost:8000
# You should see &amp;quot;Hello World!&amp;quot; from curl
docker push zihao/hello
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Now everything is ready, we will deploy this Hello World application to our Kubernetes Cluster. Save the following configuration as &lt;code&gt;deploy.yaml&lt;/code&gt;&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-YAML&#34;&gt;apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: hello
spec:
  replicas: 1
  revisionHistoryLimit: 2
  template:
    metadata:
      labels:
        app: hello
    spec:
      containers:
      - name: hello
        image: zihao/hello
        imagePullPolicy: Always
        ports:
        - containerPort: 8000
---
apiVersion: v1
kind: Service
metadata:
  name: hello
  labels:
    app: hello
spec:
  ports:
  - port: 80
    targetPort: 8000
  selector:
    app: hello
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: hello
  annotations:
    kubernetes.io/tls-acme: &amp;quot;true&amp;quot;
    kubernetes.io/ingress.class: &amp;quot;nginx&amp;quot;
spec:
  tls:
  - hosts:
    - hello.cloud.zihao.me
    secretName: hello-tls
  rules:
  - host: hello.cloud.zihao.me
    http:
      paths:
      - backend:
          serviceName: hello
          servicePort: 8000
        path: /
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;This deploy script contains 3 parts: Deployment, Service and Ingress. The deployment part will pull our pre-built container image &lt;code&gt;zihao/hello&lt;/code&gt; and run it with 1 replica. The service part describes that our container &lt;em&gt;hello&lt;/em&gt; is listening on port 8000, and creates a service &lt;em&gt;hello&lt;/em&gt; with port 80 for other containers in our cluster to access. The last Ingress part enables HTTPS traffic and says Internet traffic from &lt;code&gt;hello.cloud.zihao.me&lt;/code&gt; will be directed to our hello service at port 80. Now we will deploy this with&lt;/p&gt;

&lt;pre&gt;&lt;code&gt;kubectl apply -f deploy.yaml
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Point your domain DNS to the master server and after a while you will be able to see our example working at &lt;a href=&#34;https://hello.cloud.zihao.me&#34;&gt;https://hello.cloud.zihao.me&lt;/a&gt; YAY!&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/kube-hello.png&#34; alt=&#34;Hello World&#34; /&gt;&lt;/p&gt;

&lt;hr /&gt;

&lt;p&gt;&lt;em&gt;The hello world project is available on &lt;a href=&#34;https://gitlab.com/zzh8829/hello&#34;&gt;GitLab&lt;/a&gt;&lt;/em&gt;&lt;/p&gt;

&lt;p&gt;&lt;em&gt;Learn more about &lt;a href=&#34;https://kubernetes.io/&#34;&gt;Kubernetes&lt;/a&gt; and &lt;a href=&#34;https://docker.com&#34;&gt;Docker&lt;/a&gt;&lt;/em&gt;&lt;/p&gt;</description>
    </item>
    
  </channel>
</rss>
