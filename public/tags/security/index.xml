<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Security on Zihao Zhang</title>
    <link>https://zihao.me/tags/security/</link>
    <description>Recent content in Security on Zihao Zhang</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <copyright>Copyright © 2017 Zihao Zhang &lt;a rel=&#34;license&#34; href=&#34;https://creativecommons.org/licenses/by-nc-sa/4.0//&#34;&gt;CC BY-NC-SA 4.0&lt;/a&gt;</copyright>
    <lastBuildDate>Thu, 23 Mar 2017 23:33:17 -0700</lastBuildDate>
    <atom:link href="https://zihao.me/tags/security/" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Migrating to Password Manager</title>
      <link>https://zihao.me/post/migrating-to-password-manager/</link>
      <pubDate>Thu, 23 Mar 2017 23:33:17 -0700</pubDate>
      
      <guid>https://zihao.me/post/migrating-to-password-manager/</guid>
      <description>&lt;p&gt;Having my Spotify password stolen finally tilted me into using a password manager. This is my journey to strong and unique passwords on all my &lt;em&gt;653&lt;/em&gt; logins.
&lt;/p&gt;

&lt;h2 id=&#34;the-dark-history&#34;&gt;The Dark History&lt;/h2&gt;

&lt;p&gt;When registering for new accounts, there is occasionally that little hint about not repeating passwords. I never took it seriously since I already registered on hundreds of other websites. There is this common trick that most people use: having different password for websites grouped by the importance. I too used this strategy and had 5 levels of passwords. The weakest one was literally same as my username and the strongest one had uppercase letters, digits and special characters (BTW this is &lt;a href=&#34;https://xkcd.com/936/&#34;&gt;not very secure&lt;/a&gt;).&lt;/p&gt;

&lt;p&gt;Everything worked well in the beginning. It was around a year ago I had my level 2 password pawned because a small online forum got hacked and they stored password in plain text. Then this January I got alert for login attempts on my Skype account. And finally last week my Spotify showed an unrecognized device and random playlists were popping up. That&amp;rsquo;s when I decided my old passwords scheme was too weak and started switching to managed, strong and unique passwords.&lt;/p&gt;

&lt;h2 id=&#34;choosing-the-best-password-manager&#34;&gt;Choosing the Best Password Manager&lt;/h2&gt;

&lt;h4 id=&#34;plain-text-in-cloud-storage&#34;&gt;Plain Text in Cloud Storage&lt;/h4&gt;

&lt;p&gt;I actually had a little file of some generated passwords stored in my &lt;a href=&#34;https://seafile.zihao.me&#34;&gt;personal cloud storage&lt;/a&gt;. Although I trust the security of my cloud storage, plain text isn&amp;rsquo;t a good password storage format. On top of that,  the user experience wasn&amp;rsquo;t that great: no auto fill support, no passwords generator, etc. But it was indeed very accessible, you can do it on iCloud, Dropbox or whatever you prefer.&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/password-text.png&#34; alt=&#34;1password text&#34; /&gt;&lt;/p&gt;

&lt;h4 id=&#34;legitimate-password-manager&#34;&gt;Legitimate Password Manager&lt;/h4&gt;

&lt;p&gt;&lt;a href=&#34;https://lastpass.com&#34;&gt;LastPass&lt;/a&gt; and &lt;a href=&#34;https://1password.com&#34;&gt;1Password&lt;/a&gt; are two of the two most popular managers. LastPass has more users in general, 1Password is more popular in the developer community. They both offer desktop and mobile client and secure storage with fancy password generation and auto fill features. I tried out both apps and eventually went with 1Password. Reasons are&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;LastPass had some security problems before: &lt;a href=&#34;https://blog.lastpass.com/2015/06/lastpass-security-notice.html/&#34;&gt;2015&lt;/a&gt;, &lt;a href=&#34;https://blog.lastpass.com/2017/03/important-security-updates-for-our-users.html/&#34;&gt;2017&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;1Password has prettier interface&lt;/li&gt;
&lt;li&gt;1Password is less intrusive&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;One thing I really liked from LastPass but missing in 1Password was the &lt;a href=&#34;https://helpdesk.lastpass.com/downloading-and-installing/inbox-importer/&#34;&gt;Inbox Importer&lt;/a&gt;.&lt;/p&gt;

&lt;h2 id=&#34;migration-from-chrome&#34;&gt;Migration from Chrome&lt;/h2&gt;

&lt;p&gt;The first step is to get rid of all my old and insecure passwords from Google Chrome password storage. This is very useful because Chrome does auto-fill/login on almost all the website I commonly use, without purging them I really can&amp;rsquo;t remember to reset passwords.&lt;/p&gt;

&lt;h4 id=&#34;exporting-everything-from-chrome&#34;&gt;Exporting Everything from Chrome&lt;/h4&gt;

&lt;p&gt;This was incredibly difficult before Chrome released password exporting feature in 2016. Thanks to password exporting you can do it easily like the following&lt;/p&gt;

&lt;ul&gt;
&lt;li&gt;enable exporting at &lt;code&gt;chrome://flags/#password-import-export&lt;/code&gt;&lt;/li&gt;
&lt;li&gt;export to csv at &lt;code&gt;chrome://settings/passwords&lt;/code&gt;&lt;/li&gt;
&lt;/ul&gt;

&lt;h4 id=&#34;loading-into-1password&#34;&gt;Loading into 1Password&lt;/h4&gt;

&lt;p&gt;Importing directly will pollute the vault you already have, so we will create a new vault at &lt;a href=&#34;https://my.1password.com/vaults&#34;&gt;https://my.1password.com/vaults&lt;/a&gt; Now we can import them to 1Password in the desktop app &lt;code&gt;file -&amp;gt; import&lt;/code&gt;&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/password-import.png&#34; alt=&#34;1password import&#34; /&gt;&lt;/p&gt;

&lt;h4 id=&#34;disable-chrome-password-auto-fill&#34;&gt;Disable Chrome Password Auto-Fill&lt;/h4&gt;

&lt;p&gt;Chrome&amp;rsquo;s built in auto-fill is not needed anymore after switching to password manager. Removing them from Chrome completely also reduces the risk of side channel attacks. Go to &lt;code&gt;chrome://settings&lt;/code&gt; and use &lt;code&gt;Clear browsing data -&amp;gt; Passwords&lt;/code&gt; also uncheck Google Smart Lock to prevent the annoying pop-ups.&lt;/p&gt;

&lt;h2 id=&#34;eliminate-weak-and-duplicate-passwords&#34;&gt;Eliminate Weak and Duplicate Passwords&lt;/h2&gt;

&lt;p&gt;This is really the main reason I imported everything to 1Password. With 1Password&amp;rsquo;s built in Security Audit we can see our weak and duplicate passwords.&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/password-weak.png&#34; alt=&#34;password weak&#34; /&gt;&lt;/p&gt;

&lt;p&gt;Now we have all these information ready, your job is to fix&amp;rsquo;em all! My personal password generation setup is 32 characters long with 4 digits and 2 special characters. There really isn&amp;rsquo;t a painless way to reset everything, I recommend doing them over period of few weeks and start from the more important websites like emails and banks. In the end, protecting security needs your time and attention, tools are only here to provide some help.&lt;/p&gt;

&lt;hr /&gt;

&lt;p&gt;PS:&lt;/p&gt;

&lt;p&gt;Username and password logins are extremely inefficient and insecure. IMO, web authentication should be linked directly to email providers. If we can reset password through email, why can we just login with proof from email providers? Even then, having a master password is still the weakest point of this ecosystem. Some suggested the use of finger prints, face or retina scan. But they are closer to identification then authentication. The key point of authentication is they can&amp;rsquo;t be stolen or replicated easily. In this regard, PGP or private key based auth is ideal but probably too complicated for end users. I wish there is an alternative universe where humans are born with some biological private key. Maybe we already have that in our body somewhere waiting for us discover.&lt;/p&gt;</description>
    </item>
    
  </channel>
</rss>
