<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Artificial Intelligence on Zihao Zhang</title>
    <link>https://zihao.me/tags/artificial-intelligence/</link>
    <description>Recent content in Artificial Intelligence on Zihao Zhang</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <copyright>Copyright © 2017 Zihao Zhang &lt;a rel=&#34;license&#34; href=&#34;https://creativecommons.org/licenses/by-nc-sa/4.0//&#34;&gt;CC BY-NC-SA 4.0&lt;/a&gt;</copyright>
    <lastBuildDate>Thu, 03 Aug 2017 18:00:46 -0400</lastBuildDate>
    <atom:link href="https://zihao.me/tags/artificial-intelligence/" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Solving CartPole with Deep Q Network</title>
      <link>https://zihao.me/post/solving-cartpol-with-deep-q-network/</link>
      <pubDate>Thu, 03 Aug 2017 18:00:46 -0400</pubDate>
      
      <guid>https://zihao.me/post/solving-cartpol-with-deep-q-network/</guid>
      <description>&lt;p&gt;CartPole is the classic game where you try to balance a pole by moving it horizontally. We will try to solve this with a reinforcement learning method called Deep Q Network.
&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;images/cartpole.png&#34; alt=&#34;CartPole&#34; /&gt;&lt;/p&gt;

&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;

&lt;blockquote&gt;
&lt;p&gt;A pole is attached by an un-actuated joint to a cart, which moves along a frictionless track. The system is controlled by applying a force of +1 or -1 to the cart. The pendulum starts upright, and the goal is to prevent it from falling over. A reward of +1 is provided for every timestep that the pole remains upright. The episode ends when the pole is more than 15 degrees from vertical, or the cart moves more than 2.4 units from the center.&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;The environment provides 4 observed states: horizontal location, horizontal speed, angle, and angular speed. The agent should takes in these 4 variables and output either left or right.&lt;/p&gt;

&lt;h2 id=&#34;deep-q-network&#34;&gt;Deep Q Network&lt;/h2&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;def q_network(input_state):
    h1 = tf.layers.dense(input_state, 10, tf.nn.relu, kernel_initializer=tf.truncated_normal_initializer(stddev=0.1))
    h2 = tf.layers.dense(h1, 10, tf.nn.relu, kernel_initializer=tf.truncated_normal_initializer(stddev=0.1))
    output = tf.layers.dense(h2, 2, kernel_initializer=tf.truncated_normal_initializer(stddev=0.1))
    return output

with tf.variable_scope(&#39;state_input&#39;):
    state_in = tf.placeholder(tf.float32, [None, 4])

with tf.variable_scope(&#39;eval_weights&#39;):
    q_value = q_network(state_in)

with tf.variable_scope(&#39;state_next_input&#39;):
    state_next_in = tf.placeholder(tf.float32, [None, 4])

with tf.variable_scope(&#39;target_weights&#39;):
    q_target = q_network(state_next_in)

weights = tf.get_collection(tf.GraphKeys.VARIABLES, &#39;eval_weights&#39;)
target_weights = tf.get_collection(tf.GraphKeys.VARIABLES, &#39;target_weights&#39;)
update_target_weight = [tf.assign(wt, w) for wt,w in zip(target_weights, weights)]

with tf.variable_scope(&#39;q_target_input&#39;):
    q_target_in = tf.placeholder(tf.float32, [None, 2])

with tf.variable_scope(&#39;loss&#39;):
    loss = tf.nn.l2_loss(q_target_in - q_value)

with tf.variable_scope(&#39;train&#39;):
    train = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

tf_steps = tf.placeholder(tf.int32)
loss_smr = tf.summary.scalar(&#39;l2_loss&#39;, loss)
step_smr = tf.summary.scalar(&#39;steps&#39;, tf_steps)
&lt;/code&gt;&lt;/pre&gt;

&lt;h2 id=&#34;result&#34;&gt;Result&lt;/h2&gt;

&lt;p&gt;&lt;img src=&#34;images/cartpole-result.png&#34; alt=&#34;CartPole Result&#34; /&gt;&lt;/p&gt;

&lt;p&gt;We see that the network reached average of 200 steps per episode pretty quickly.&lt;/p&gt;

&lt;hr /&gt;

&lt;p&gt;CartPole on OpenAI: &lt;a href=&#34;https://gym.openai.com/envs/CartPole-v0&#34;&gt;https://gym.openai.com/envs/CartPole-v0&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;Deep Q Network: &lt;a href=&#34;https://deepmind.com/research/dqn/&#34;&gt;https://deepmind.com/research/dqn/&lt;/a&gt;&lt;/p&gt;</description>
    </item>
    
  </channel>
</rss>
