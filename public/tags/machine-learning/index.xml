<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Machine Learning on Zihao Zhang</title>
    <link>https://zihao.me/tags/machine-learning/</link>
    <description>Recent content in Machine Learning on Zihao Zhang</description>
    <generator>Hugo -- gohugo.io</generator>
    <language>en-us</language>
    <copyright>Copyright © 2017 Zihao Zhang &lt;a rel=&#34;license&#34; href=&#34;https://creativecommons.org/licenses/by-nc-sa/4.0//&#34;&gt;CC BY-NC-SA 4.0&lt;/a&gt;</copyright>
    <lastBuildDate>Thu, 03 Aug 2017 18:00:46 -0400</lastBuildDate>
    <atom:link href="https://zihao.me/tags/machine-learning/" rel="self" type="application/rss+xml" />
    
    <item>
      <title>Solving CartPole with Deep Q Network</title>
      <link>https://zihao.me/post/solving-cartpol-with-deep-q-network/</link>
      <pubDate>Thu, 03 Aug 2017 18:00:46 -0400</pubDate>
      
      <guid>https://zihao.me/post/solving-cartpol-with-deep-q-network/</guid>
      <description>&lt;p&gt;CartPole is the classic game where you try to balance a pole by moving it horizontally. We will try to solve this with a reinforcement learning method called Deep Q Network.
&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;images/cartpole.png&#34; alt=&#34;CartPole&#34; /&gt;&lt;/p&gt;

&lt;h2 id=&#34;description&#34;&gt;Description&lt;/h2&gt;

&lt;blockquote&gt;
&lt;p&gt;A pole is attached by an un-actuated joint to a cart, which moves along a frictionless track. The system is controlled by applying a force of +1 or -1 to the cart. The pendulum starts upright, and the goal is to prevent it from falling over. A reward of +1 is provided for every timestep that the pole remains upright. The episode ends when the pole is more than 15 degrees from vertical, or the cart moves more than 2.4 units from the center.&lt;/p&gt;
&lt;/blockquote&gt;

&lt;p&gt;The environment provides 4 observed states: horizontal location, horizontal speed, angle, and angular speed. The agent should takes in these 4 variables and output either left or right.&lt;/p&gt;

&lt;h2 id=&#34;deep-q-network&#34;&gt;Deep Q Network&lt;/h2&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;def q_network(input_state):
    h1 = tf.layers.dense(input_state, 10, tf.nn.relu, kernel_initializer=tf.truncated_normal_initializer(stddev=0.1))
    h2 = tf.layers.dense(h1, 10, tf.nn.relu, kernel_initializer=tf.truncated_normal_initializer(stddev=0.1))
    output = tf.layers.dense(h2, 2, kernel_initializer=tf.truncated_normal_initializer(stddev=0.1))
    return output

with tf.variable_scope(&#39;state_input&#39;):
    state_in = tf.placeholder(tf.float32, [None, 4])

with tf.variable_scope(&#39;eval_weights&#39;):
    q_value = q_network(state_in)

with tf.variable_scope(&#39;state_next_input&#39;):
    state_next_in = tf.placeholder(tf.float32, [None, 4])

with tf.variable_scope(&#39;target_weights&#39;):
    q_target = q_network(state_next_in)

weights = tf.get_collection(tf.GraphKeys.VARIABLES, &#39;eval_weights&#39;)
target_weights = tf.get_collection(tf.GraphKeys.VARIABLES, &#39;target_weights&#39;)
update_target_weight = [tf.assign(wt, w) for wt,w in zip(target_weights, weights)]

with tf.variable_scope(&#39;q_target_input&#39;):
    q_target_in = tf.placeholder(tf.float32, [None, 2])

with tf.variable_scope(&#39;loss&#39;):
    loss = tf.nn.l2_loss(q_target_in - q_value)

with tf.variable_scope(&#39;train&#39;):
    train = tf.train.AdamOptimizer(learning_rate=0.001).minimize(loss)

tf_steps = tf.placeholder(tf.int32)
loss_smr = tf.summary.scalar(&#39;l2_loss&#39;, loss)
step_smr = tf.summary.scalar(&#39;steps&#39;, tf_steps)
&lt;/code&gt;&lt;/pre&gt;

&lt;h2 id=&#34;result&#34;&gt;Result&lt;/h2&gt;

&lt;p&gt;&lt;img src=&#34;images/cartpole-result.png&#34; alt=&#34;CartPole Result&#34; /&gt;&lt;/p&gt;

&lt;p&gt;We see that the network reached average of 200 steps per episode pretty quickly.&lt;/p&gt;

&lt;hr /&gt;

&lt;p&gt;CartPole on OpenAI: &lt;a href=&#34;https://gym.openai.com/envs/CartPole-v0&#34;&gt;https://gym.openai.com/envs/CartPole-v0&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;Deep Q Network: &lt;a href=&#34;https://deepmind.com/research/dqn/&#34;&gt;https://deepmind.com/research/dqn/&lt;/a&gt;&lt;/p&gt;</description>
    </item>
    
    <item>
      <title>Basic Neural Network for MNIST with Keras</title>
      <link>https://zihao.me/post/basic-neural-network-for-mnist-with-keras/</link>
      <pubDate>Wed, 29 Mar 2017 15:26:24 -0700</pubDate>
      
      <guid>https://zihao.me/post/basic-neural-network-for-mnist-with-keras/</guid>
      <description>&lt;p&gt;This is a simple tutorial on a basic 97% accurate neural network model for MNIST digit classification. This model contains multiple RELU layers and dropouts layers with a softmax layer for prediction.
&lt;/p&gt;

&lt;p&gt;Only less than 30 seconds of training time is required on my 15 inch Macbook. I strongly recommend using jupyter notebook, but normal python terminal is fine too.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-bash&#34;&gt;# install some libraries if don&#39;t have them
pip3 install numpy pandas scipy sklearn tensorflow keras matplotlib
&lt;/code&gt;&lt;/pre&gt;

&lt;h3 id=&#34;setting-up-python-environment&#34;&gt;Setting up python environment&lt;/h3&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
&lt;/code&gt;&lt;/pre&gt;

&lt;h3 id=&#34;loading-data-sets&#34;&gt;Loading data sets&lt;/h3&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# List our data sets
from subprocess import check_output
print(check_output([&amp;quot;ls&amp;quot;, &amp;quot;data&amp;quot;]).decode(&amp;quot;utf8&amp;quot;))

# Loading data with Pandas
train = pd.read_csv(&#39;data/train.csv&#39;)
train_images = train.ix[:,1:].values.astype(&#39;float32&#39;)
train_labels = train.ix[:,0].values.astype(&#39;int32&#39;)

test_images = pd.read_csv(&#39;data/test.csv&#39;).values.astype(&#39;float32&#39;)

print(train_images.shape, train_labels.shape, test_images.shape)
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;output:
sample_submission.csv
test.csv
train.csv

(42000, 784) (42000,) (28000, 784)
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# Show samples from training data
show_images = train_images.reshape(train_images.shape[0], 28, 28)
n = 3
row = 3
begin = 42
for i in range(begin,begin+n):
    plt.subplot(n//row, row, i%row+1)
    plt.imshow(show_images[i], cmap=plt.get_cmap(&#39;gray&#39;))
    plt.title(train_labels[i])
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/mnist-output_3_0.png&#34; alt=&#34;number images&#34; /&gt;&lt;/p&gt;

&lt;h3 id=&#34;process-data-for-training&#34;&gt;Process data for training&lt;/h3&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# Normalize pixel values from [0, 255] to [0, 1]
train_images = train_images / 255
test_images = test_images / 255

# Convert labels from [0, 9] to one-hot representation.
from keras.utils.np_utils import to_categorical
train_labels = to_categorical(train_labels)

print(train_labels[0])
print(train_images.shape, train_labels.shape)
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;output:
[ 0.  1.  0.  0.  0.  0.  0.  0.  0.  0.]
(42000, 784) (42000, 10)
&lt;/code&gt;&lt;/pre&gt;

&lt;h3 id=&#34;training-neural-network&#34;&gt;Training neural network&lt;/h3&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# Create a basic neural network
# 64 relu -&amp;gt; 128 relu -&amp;gt; dropout 0.15
# -&amp;gt; 64 relu -&amp;gt; dropout 0.15 -&amp;gt; softmax 10 
from keras.models import Sequential
from keras.layers import Dense , Dropout

model=Sequential()
model.add(Dense(64,activation=&#39;relu&#39;,input_dim=(28 * 28)))
model.add(Dense(128,activation=&#39;relu&#39;))
model.add(Dropout(0.15))
model.add(Dense(64, activation=&#39;relu&#39;))
model.add(Dropout(0.15))
model.add(Dense(10,activation=&#39;softmax&#39;))

from keras.optimizers import RMSprop
model.compile(optimizer=RMSprop(lr=0.001),
    loss=&#39;categorical_crossentropy&#39;,
    metrics=[&#39;accuracy&#39;])
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# Train our model with 15 steps using 90% for training and 10% for cross validation
history=model.fit(train_images, train_labels, validation_split = 0.1, 
            epochs=15, batch_size=64)
&lt;/code&gt;&lt;/pre&gt;

&lt;pre&gt;&lt;code&gt;output:
Train on 37800 samples, validate on 4200 samples
Epoch 1/15
37800/37800 [==============================] - 2s - loss: 0.4146 - acc: 0.8743 - val_loss: 0.1901 - val_acc: 0.9419

Epoch 2/15
37800/37800 [==============================] - 2s - loss: 0.1828 - acc: 0.9454 - val_loss: 0.1334 - val_acc: 0.9581

Epoch 3/15
37800/37800 [==============================] - 2s - loss: 0.1359 - acc: 0.9608 - val_loss: 0.1330 - val_acc: 0.9614

Epoch 4/15
37800/37800 [==============================] - 2s - loss: 0.1071 - acc: 0.9680 - val_loss: 0.1217 - val_acc: 0.9671

Epoch 5/15
37800/37800 [==============================] - 2s - loss: 0.0907 - acc: 0.9731 - val_loss: 0.1281 - val_acc: 0.9674

Epoch 6/15
37800/37800 [==============================] - 2s - loss: 0.0774 - acc: 0.9770 - val_loss: 0.1091 - val_acc: 0.9695

Epoch 7/15
37800/37800 [==============================] - 2s - loss: 0.0701 - acc: 0.9799 - val_loss: 0.1146 - val_acc: 0.9712

Epoch 8/15
37800/37800 [==============================] - 3s - loss: 0.0637 - acc: 0.9813 - val_loss: 0.1338 - val_acc: 0.9679

Epoch 9/15
37800/37800 [==============================] - 2s - loss: 0.0594 - acc: 0.9825 - val_loss: 0.1217 - val_acc: 0.9726

Epoch 10/15
37800/37800 [==============================] - 2s - loss: 0.0519 - acc: 0.9851 - val_loss: 0.1245 - val_acc: 0.9736

Epoch 11/15
37800/37800 [==============================] - 2s - loss: 0.0484 - acc: 0.9858 - val_loss: 0.1435 - val_acc: 0.9705

Epoch 12/15
37800/37800 [==============================] - 2s - loss: 0.0451 - acc: 0.9870 - val_loss: 0.1442 - val_acc: 0.9724

Epoch 13/15
37800/37800 [==============================] - 2s - loss: 0.0442 - acc: 0.9872 - val_loss: 0.1412 - val_acc: 0.9702

Epoch 14/15
37800/37800 [==============================] - 2s - loss: 0.0393 - acc: 0.9889 - val_loss: 0.1361 - val_acc: 0.9738

Epoch 15/15
37800/37800 [==============================] - 2s - loss: 0.0398 - acc: 0.9887 - val_loss: 0.1522 - val_acc: 0.9726
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;From this output we can see our model is 98.87% accurate on training set and 97.26% accurate on cross validation set.&lt;/p&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# Graphing Loss on the left and Accuracy on the right
history_dict = history.history

epochs = range(1, 16)

plt.rcParams[&amp;quot;figure.figsize&amp;quot;] = [10,5]
plt.subplot(121)

loss_values = history_dict[&#39;loss&#39;]
val_loss_values = history_dict[&#39;val_loss&#39;]
plt.plot(epochs, loss_values, &#39;bo&#39;)
plt.plot(epochs, val_loss_values, &#39;ro&#39;)
plt.xlabel(&#39;Epochs&#39;)
plt.ylabel(&#39;Loss&#39;)

plt.subplot(122)

acc_values = history_dict[&#39;acc&#39;]
val_acc_values = history_dict[&#39;val_acc&#39;]
plt.plot(epochs, acc_values, &#39;bo&#39;)
plt.plot(epochs, val_acc_values, &#39;ro&#39;)
plt.xlabel(&#39;Epochs&#39;)
plt.ylabel(&#39;Accuracy&#39;)

plt.show()
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/mnist-output_9_0.png&#34; alt=&#34;history plot&#34; /&gt;&lt;/p&gt;

&lt;h3 id=&#34;make-prediction&#34;&gt;Make prediction&lt;/h3&gt;

&lt;pre&gt;&lt;code class=&#34;language-python&#34;&gt;# Generate prediction for test set
predictions = model.predict_classes(test_images, verbose=0)

submissions=pd.DataFrame({&amp;quot;ImageId&amp;quot;: list(range(1,len(predictions)+1)),
                         &amp;quot;Label&amp;quot;: predictions})
submissions.to_csv(&amp;quot;predictions.csv&amp;quot;, index=False, header=True)
&lt;/code&gt;&lt;/pre&gt;

&lt;p&gt;Everything all together takes about 5 minutes, pretty good for 97% accuracy. There are many ways to improve the accuracy, I might write about them in future. The goal here is just to have a basic working setup and test submissions on Kaggle.&lt;/p&gt;

&lt;p&gt;&lt;img src=&#34;https://zihao.me/images/mnist-kaggle.png&#34; alt=&#34;kaggle result&#34; /&gt;&lt;/p&gt;

&lt;p&gt;With this simple model we achieved 97.257% accuracy on test set.&lt;/p&gt;

&lt;hr /&gt;

&lt;p&gt;Thanks Poonam Ligade for posting her &lt;a href=&#34;https://www.kaggle.com/poonaml/digit-recognizer/deep-neural-network-keras-way&#34;&gt;Kaggle Kernel&lt;/a&gt;&lt;/p&gt;

&lt;p&gt;Try this problem on &lt;a href=&#34;https://www.kaggle.com/c/digit-recognizer&#34;&gt;Kaggle&lt;/a&gt;&lt;/p&gt;</description>
    </item>
    
  </channel>
</rss>
